const buttonSave = document.querySelector('#button-submit')

document.querySelectorAll('input')
  .forEach(element => {
    element.addEventListener('change', e => {
      buttonSaveChanged()
      if (e.target.id.startsWith('checkbox-feature-')) {
        const id = '.feature-' + e.target.id.split('-').pop();
        featureActivation(id, e.target.checked)
      }
    })
  })

document.addEventListener('DOMContentLoaded', restoreOptions)
document.querySelector('form').addEventListener('submit', saveOptions)

/////////

/**
 * For all children elements of the given id : greys out everything and deactivate inputs.
 *
 * @param {string} selector Selector of element to activate/deactivate
 * @param {boolean} active Active or not
 */
 function featureActivation(selector, active) {
    document.querySelectorAll(selector + ' input')
      .forEach(input => active ? input.removeAttribute('disabled') : input.setAttribute('disabled', true))

    document.querySelectorAll(selector + ' *')
      .forEach(el => el.style.opacity = active ? 1 : 0.6)

    document.querySelectorAll(selector)
      .forEach(element => element.classList.toggle('active', active))

    if (selector === '.feature-coloring' || selector === '.feature-tooltip') {
      const coloring = document.querySelector('#checkbox-feature-coloring')
      const tooltip = document.querySelector('#checkbox-feature-tooltip')
      featureActivation('.feature-thumbs-requirements', coloring.checked || tooltip.checked)
    }
}

/**
 * Reload colors to show them directly without reopening the options window
 */
function reloadColors() {
  document.querySelectorAll('.colored-input')
    .forEach(group => {
      const input = group.querySelector('input')
      const label = group.querySelector('label')
      label.style.color = input.value
      input.style.color = input.value
    })
}

function refreshFeatures() {
  document.querySelectorAll('.features input')
    .forEach(feature => {
      if (feature.id.startsWith('checkbox-feature-')) {
        const id = '.feature-' + feature.id.split('-').pop();
        featureActivation(id, feature.checked)
      }
    })
}

function buttonSaveChanged() {
  buttonSave.innerText = 'Save'
  buttonSave.classList = 'changed'
}

/////////

function saveOptions(e) {
  if (e) {
    e.preventDefault()
  }

  const ticketLink = document.querySelector('#ticketing-input').value
  const teammates =  document.querySelector('#teammates-highlight-input').value

  const store = {
    store: {
      features: {
        ticketing: {
          active: document.querySelector('#checkbox-feature-ticketing').checked,
          options: {
            link: ticketLink ? ticketLink.trim() : 'https://your-jira.atlassian.net/browse/'
          }
        },
        authors: {
          active: document.querySelector('#checkbox-feature-authors').checked,
          options: {
            teammates: teammates ? teammates.trim() : ''
          }
        },
        coloring: {
          active: document.querySelector('#checkbox-feature-coloring').checked,
          options: {
            approvalDefault: parseInt(document.querySelector('#approval-needed-default-input').value || '2'),
            approvalBugfix: parseInt(document.querySelector('#approval-needed-bugfix-input').value || '1'),
            approvalSprintbug: parseInt(document.querySelector('#approval-needed-sprintbug-input').value || '1'),
            colorDefault: document.querySelector('#color-default').value || 'inherit',
            colorInProgress: document.querySelector('#color-in-progress').value || '#d98c00',
            colorReady: document.querySelector('#color-ready').value || '#2684ff',
            colorFailed: document.querySelector('#color-failed').value || '#b34d60'
          }
        },
        tooltip: {
          active: document.querySelector('#checkbox-feature-tooltip').checked
        }
      }
    }
  }

  chrome.storage.sync.set(store)

  buttonSave.innerText = 'Saved'
  buttonSave.classList = 'success'

  reloadColors()

  return store
}

function restoreOptions() {
  function setCurrentChoices(result) {
    if (!result?.store) {
      // if first installation of plugin, we force the default values
      document.querySelector('#checkbox-feature-ticketing').checked = true
      document.querySelector('#checkbox-feature-authors').checked = true
      document.querySelector('#checkbox-feature-coloring').checked = true
      document.querySelector('#checkbox-feature-tooltip').checked = true

      result = saveOptions()
    }

    // features activations
    document.querySelector('#checkbox-feature-ticketing').checked = result.store.features.ticketing.active
    document.querySelector('#checkbox-feature-authors').checked = result.store.features.authors.active
    document.querySelector('#checkbox-feature-coloring').checked = result.store.features.coloring.active
    document.querySelector('#checkbox-feature-tooltip').checked = result.store.features.tooltip.active

    // feature ticketing
    document.querySelector('#ticketing-input').value = result.store.features.ticketing.options.link

    // feature authors
    document.querySelector('#teammates-highlight-input').value = result.store.features.authors.options.teammates

    // feature coloring
    document.querySelector('#approval-needed-default-input').value = result.store.features.coloring.options.approvalDefault
    document.querySelector('#approval-needed-bugfix-input').value = result.store.features.coloring.options.approvalBugfix
    document.querySelector('#approval-needed-sprintbug-input').value = result.store.features.coloring.options.approvalSprintbug
    document.querySelector('#color-default').value = result.store.features.coloring.options.colorDefault
    document.querySelector('#color-in-progress').value = result.store.features.coloring.options.colorInProgress
    document.querySelector('#color-ready').value = result.store.features.coloring.options.colorReady
    document.querySelector('#color-failed').value = result.store.features.coloring.options.colorFailed

    reloadColors()
    refreshFeatures()
  }

  chrome.storage.sync.get('store', setCurrentChoices)
}
