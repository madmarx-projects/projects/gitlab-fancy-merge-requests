# Gitlab fancy merge request

## Links

- https://addons.mozilla.org/fr/firefox/addon/gitlab-fancy-merge-requests/
- https://chrome.google.com/webstore/detail/gitlab-fancy-merge-reques/adjnbgiifacplplnjckboglaacaicbph

## Description

If you use Gitlab with your team and you want to see who approved the merge requests, you might need this extension.
Also, if your merge request mention a user story code (eg. TICKET-1234), this extension will display a link to that ticket. Neat.
