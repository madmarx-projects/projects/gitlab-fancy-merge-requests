const Config = {
  style: {
    MR_UNTOUCHED_EMOJI: "",
    MR_IN_PROGRESS_EMOJI: "",
    MR_READY_EMOJI: "👍",
    MR_FAILED_EMOJI: "⛔",
    MR_LOADING_EMOJI: "⏱",
    MR_BACKGROUND: "aliceblue", // light blue
    BUTTON_JIRA_COLOR: "#2684ff", // blue jira
    BUTTON_JIRA_TEXT_COLOR: "white"
  },
  gitlabDomClasses: {
    MR_LIST: ".mr-list",
    MR_ELEMENT: "li.merge-request",
    MR_LINK: ".merge-request-title a",
    MR_DETAILS: ".merge-request-details",
    MR_DETAILS_TITLE: ".detail-page-description h2",
    MR_VOTES_CONTAINER: ".controls",
    UP_VOTES: "issuable-upvotes",
    DOWN_VOTES: "issuable-downvotes",
    MERGE_TITLE: "qa-issuable-form-title",
    CURRENT_USERNAME: ".gl-new-dropdown-item-content span.gl-font-bold",
    AUTHOR: ".author",
    AUTHOR_BLOCK: ".issuable-authored",
    BADGE_YOU: "badge-pill badge-success gl-badge gl-ml-2 sm",
    BADGE_TEAMMATE: "badge-pill badge-info gl-badge gl-ml-2 sm"
  },
  regexes: {
    TICKET_REF: /([a-zA-Z]+|[iI]18[nN])-\d{1,5}/,
    BUGFIX: /bugfix/i,
    SPRINTBUG: /SB/
  }
}

//////////////// UTILS FUNCTIONS ////////////////

/**
 * Builds an object with default values for a first init of local storage.
 * Used at first install, when storage has never been initialized.
 *
 * @returns {object} default storage
 */
function getDefaultStorage() {
  return {
    features: {
      ticketing: {
        active: true,
        options: {
          link: 'https://your-jira.atlassian.net/browse/'
        }
      },
      authors: {
        active: true,
        options: {
          teammates: ''
        }
      },
      coloring: {
        active: true,
        options: {
          approvalDefault: 2,
          approvalBugfix: 1,
          approvalSprintbug: 1,
          colorDefault: 'inherit',
          colorInProgress: '#d98c00',
          colorReady: '#2684ff',
          colorFailed: '#b34d60'
        }
      },
      tooltip: {
        active: true
      }
    }
  }
}

/**
 * Adds some more CSS to the page
 */
function loadCustomCss() {
  let css = `
    .btn-ticketing {
      padding: 2px 7px;
      border-radius: 3px;
      background-color: ${Config.style.BUTTON_JIRA_COLOR};
      color: ${Config.style.BUTTON_JIRA_TEXT_COLOR} !important;
      text-decoration: none !important;
    }
    .btn-ticketing:hover {
      opacity: .7;
    }

    a.mr-untouched {
      color: ${storage.features.coloring.options.colorDefault} !important;
    }
    a.mr-untouched::before {
      content: '${Config.style.MR_UNTOUCHED_EMOJI} ';
    }

    a.mr-in-progress {
      color: ${storage.features.coloring.options.colorInProgress} !important;
    }
    a.mr-in-progress::before {
      content: '${Config.style.MR_IN_PROGRESS_EMOJI} ';
    }

    a.mr-ready {
      color: ${storage.features.coloring.options.colorReady} !important;
    }
    a.mr-ready::before,
    a.mr-has-enough-thumbs::before {
      content: '${Config.style.MR_READY_EMOJI} ';
    }

    a.mr-failed {
      color: ${storage.features.coloring.options.colorFailed} !important;
    }
    a.mr-failed::before {
      content: '${Config.style.MR_FAILED_EMOJI} ';
    }

    a.mr-loading::after {
      content: ' ${ Config.style.MR_LOADING_EMOJI}';
    }

    li.merge-request:hover {
      background-color: ${Config.style.MR_BACKGROUND};
    }
    .issuable-meta {
      overflow: unset;
    }
  `

  if (currentBrowser === 'chrome') {
    css += `
        .merge-request-title a {
          font-weight: 500;
        }
      `
  }

  const body = document.getElementsByTagName('body')[0]
  const style = document.createElement('style')
  style.innerHTML = css
  body.appendChild(style)
}

/**
 * Show loading information
 *
 * @param {HTMLElement} element full merge request HTML element
 * @param {HTMLElement} linkElement merge request link HTML element
 * @param {boolean} loading Loading or not
 */
function loading(element, linkElement, loading) {
  if (loading) {
    linkElement.classList.add('mr-loading')
    element.title = 'loading...'
  } else {
    linkElement.classList.remove('mr-loading')
    element.title = ''
  }
}

/**
 * Call specific URL to fetch the current project_id
 *
 * @param {HTMLElement} element The complete list of merge requests
 * @returns {number} The current project_id
 */
async function getProjectId(element) {
  const firstLink = element.querySelector(Config.gitlabDomClasses.MR_LINK)

  let res = -1
  try {
    const json = await fetch(`${firstLink.href}/cached_widget.json`)
      .then(call => call.json())
    res = json.source_project_id
  } catch (e) {
    console.error('Cannot fetch project id', e)
  }

  return res
}

/**
 * Call specific URL to fetch the current state of open threads
 *
 * @param {HTMLElement} element merge request link HTML element
 * @returns {object} An object with merge request's threads state
 */
async function getThreadsState(element) {
  const url = `${element.href}/discussions.json`

  let data
  try {
    data = await fetch(url)
      .then(call => call.json())
  } catch (e) {
    console.error('Error while fetching discussion info', e)
  }

  const res = {
    nbThreads: 0,
    nbThreadsResolved: 0,
    allThreadsResolved: true
  }

  if (data.length > 0) {
    res.nbThreads = data.reduce((acc, el) => acc + (el.resolvable ? 1 : 0), 0)
    res.nbThreadsResolved = data.reduce((acc, el) => acc + (el.resolvable && el.resolved ? 1 : 0), 0)
    res.allThreadsResolved = res.nbThreadsResolved === res.nbThreads
  }

  return res
}

/**
 * Call specific URL to fetch the current state of approvals
 *
 * @param {HTMLElement} element merge request link HTML element
 * @returns {object} An object with merge request's approvals state
 */
async function getApprovalsState(element, idProject) {
  const idMergeRequest = element.href.split('/').pop()
  const url = `${element.origin}/api/v4/projects/${idProject}/merge_requests/${idMergeRequest}/award_emoji`

  let data
  try {
    data = await fetch(url)
      .then(call => call.json())
  } catch (e) {
    console.error('Error while fetching approvals info', e)
  }

  const res = {
    thumbsup: {
      nb: 0,
      names: []
    },
    thumbsdown: {
      nb: 0,
      names: []
    }
  }

  if (data.length > 0) {
    res.thumbsup.names = data.filter(a => a.name === 'thumbsup').map(a => a.user.name)
    res.thumbsup.nb = res.thumbsup.names.length
    res.thumbsdown.names = data.filter(a => a.name === 'thumbsdown').map(a => a.user.name)
    res.thumbsdown.nb = res.thumbsdown.names.length
  }

  return res
}

/**
 * Returns the state of merge request title match against the various regexes
 *
 * @param {HTMLElement} element merge request link HTML element
 * @returns {object} Regex match state
 */
function getMergeRequestRegexMatch(element) {
  const linkText = element.textContent.trim()

  return {
    isBugfix: linkText.match(Config.regexes.BUGFIX) !== null,
    isSprintBug: linkText.match(Config.regexes.SPRINTBUG) !== null
  }
}

/**
 * Check if text matches the ticket regex
 *
 * @param {string} text Text to check regex on
 * @returns {string | null} The matched string or null if not found
 */
function getTicketRefMatch(text) {
  const hasTicketMatch = text.match(Config.regexes.TICKET_REF)
  return hasTicketMatch !== null ? hasTicketMatch[0].toUpperCase() : null
}

//////////////// FEATURES FUNCTIONS ////////////////

/**
 * Show ticketing button in merge request list page
 *
 * @param {HTMLElement} element the HTML element to append the ticketing button
 * @param {string} title Text to check regex on
 */
function showTicketButtonInListPage(element, title) {
  const ticket = getTicketRefMatch(title)
  if (ticket) {
    const url = `${storage.features.ticketing.options.link}${ticket}`
    const buttonLi = document.createElement('li')
    const link = document.createElement('a')
    link.classList.add('btn-ticketing')
    link.href = url
    link.title = url
    link.innerText = ticket
    buttonLi.appendChild(link)
    element.appendChild(buttonLi)
  }
}

/**
 * Show ticketing button near title in merge request details page
 *
 * @param {HTMLElement} element the HTML element to append the ticketing button
 * @param {string} title Text to check regex on
 */
function showTicketButtonInDetailsPage(element, title) {
  const ticket = getTicketRefMatch(title)
  if (ticket) {
    const url = `${storage.features.ticketing.options.link}${ticket}`
    const button = document.createElement('span')
    const link = document.createElement('a')
    link.classList.add('btn-ticketing')
    link.href = url
    link.title = url
    link.style = 'float: right;font-size: 0.6em;font-weight: normal;'
    link.innerText = ticket
    button.appendChild(link)
    element.appendChild(button)
  }
}

/**
 * Highlights author's name if it's you or a member of your team
 *
 * @param {HTMLElement} element full merge request HTML element
 * @param {string} currentUserName Name of the connected user (in lowercase)
 * @param {array} teammates Array of strings representing teammates names (in lowercase)
 */
function highlightAuthors(element, currentUserName, teammates) {
  const author = element.querySelector(Config.gitlabDomClasses.AUTHOR).innerText.trim().toLowerCase()

  if (author === currentUserName) {
    addBadgeYou(element)
  } else if (teammates.find(el => author === el)) {
    addBadgeTeammate(element)
  }
}

/**
 * Adds a success badge near your name
 *
 * @param {HTMLELement} element full merge request HTML element
 */
function addBadgeYou(element) {
  const span = document.createElement('span')
  span.classList = Config.gitlabDomClasses.BADGE_YOU
  span.innerText = 'it\'s you'

  element.querySelector(Config.gitlabDomClasses.AUTHOR_BLOCK).appendChild(span)
}

/**
 * Adds a info badge near your teammates name
 *
 * @param {HTMLELement} element full merge request HTML element
 */
function addBadgeTeammate(element) {
  const span = document.createElement('span')
  span.classList = Config.gitlabDomClasses.BADGE_TEAMMATE
  span.innerText = 'teammate'

  element.querySelector(Config.gitlabDomClasses.AUTHOR_BLOCK).appendChild(span)
}

/**
 * Colorize merge request based on info available on the page locally. We build an pseudo-artificial mergeRequestState.
 *
 * @param {HTMLElement} element full merge request HTML element
 * @param {object} mergeRequestState state of the current merge request
 */
function quickColorizeMergeRequestLink(element, mergeRequestState) {
  for (let el of element.querySelector(Config.gitlabDomClasses.MR_VOTES_CONTAINER).children) {
    if (el.classList.contains(Config.gitlabDomClasses.UP_VOTES)) {
      mergeRequestState.thumbsup.nb = parseInt(el.textContent.trim())
    }

    if (el.classList.contains(Config.gitlabDomClasses.DOWN_VOTES)) {
      mergeRequestState.thumbsdown.nb = parseInt(el.textContent.trim())
    }
  }

  const mergeRequestLinkElement = element.querySelector(Config.gitlabDomClasses.MR_LINK)

  colorizeMergeRequestLink(mergeRequestLinkElement, mergeRequestState)
}

/**
 * Colorize merge request based on merge request state
 *
 * @param {HTMLElement} element merge request link HTML element
 * @param {object} mergeRequestState state of the current merge request
 */
function colorizeMergeRequestLink(element, mergeRequestState) {
  const nbApproval = mergeRequestState.thumbsup.nb

  let hasEnoughVotes = false

  if ((!mergeRequestState.isBugfix && !mergeRequestState.isSprintBug && nbApproval >= storage.features.coloring.options.approvalDefault) ||
    (mergeRequestState.isBugfix && nbApproval >= storage.features.coloring.options.approvalBugfix) ||
    (mergeRequestState.isSprintBug && nbApproval >= storage.features.coloring.options.approvalSprintbug)) {
    hasEnoughVotes = true
  }

  if (mergeRequestState.thumbsdown.nb > 0) {
    element.classList = 'mr-failed'
  } else if (hasEnoughVotes && mergeRequestState.allThreadsResolved) {
    element.classList = 'mr-ready'
  } else if (mergeRequestState.nbThreads > 0 || mergeRequestState.thumbsup.nb > 0) {
    element.classList = 'mr-in-progress'
    if (hasEnoughVotes) {
      element.classList.add('mr-has-enough-thumbs')
    }
  } else {
    element.classList = 'mr-untouched'
  }
}

/**
 * Builds a string with the state of the merge request => what's needed + thumbs + threads
 *
 * @param {object} mergeRequestState state of the current merge request
 */
function buildMergeRequestTooltip(mergeRequestState) {
  // needs
  const thumbsRequired = mergeRequestState.isBugfix ? storage.features.coloring.options.approvalBugfix : (mergeRequestState.isSprintBug ? storage.features.coloring.options.approvalSprintbug : storage.features.coloring.options.approvalDefault)
  const needsArray = [
    thumbsRequired - mergeRequestState.thumbsup.nb > 0 ? '' + (thumbsRequired - mergeRequestState.thumbsup.nb) + ' more 👍' : '',
    mergeRequestState.thumbsdown.nb > 0 ? mergeRequestState.thumbsdown.nb + ' less 👎' : '',
    !mergeRequestState.allThreadsResolved ? (mergeRequestState.nbThreads - mergeRequestState.nbThreadsResolved) + ' thread' + (mergeRequestState.nbThreads - mergeRequestState.nbThreadsResolved > 1 ? 's' : '') + ' to be resolved' : ''
  ]
  const filteredNeedsArray = needsArray.filter(str => str !== '')
  const filteredNeedsStr = filteredNeedsArray.length < 2 ? filteredNeedsArray.join(' and ') : filteredNeedsArray.slice(0, -1).join(', ') + ' and ' + filteredNeedsArray.slice(-1)[0]
  const needsStr = filteredNeedsArray.length > 0 ? '⛔ Waiting for ' + filteredNeedsStr : ''

  // thumbs
  let thumbsupListStr = ''
  if (mergeRequestState.thumbsup.nb > 0) {
    thumbsupListStr = '👍x' + mergeRequestState.thumbsup.nb + ' ' + mergeRequestState.thumbsup.names.join(', ')
  }
  let thumbsdownListStr = ''
  if (mergeRequestState.thumbsdown.nb > 0) {
    thumbsdownListStr = '👎x' + mergeRequestState.thumbsdown.nb + ' ' + mergeRequestState.thumbsdown.names.join(', ')
  }

  // threads
  const threadsStr = (mergeRequestState.nbThreads === 0 ? '✅ No open thread' : (mergeRequestState.allThreadsResolved ? '✅ ' : 'ℹ️ ') + mergeRequestState.nbThreadsResolved + '/' + mergeRequestState.nbThreads + ' resolved threads')

  return [needsStr, thumbsupListStr, thumbsdownListStr, threadsStr].filter(str => str !== '').join('\n')
}

//////////////// MAIN ////////////////

var storage = {}

var currentBrowser = 'firefox' // 'firefox' | 'chrome'
if (typeof browser === 'undefined') {
  currentBrowser = 'chrome'
}

const body = document.body

if (body && document.location.origin.includes('gitlab')) {
  main()
}

async function main() {
  const mergeRequestList = document.querySelector(Config.gitlabDomClasses.MR_LIST)
  const mergeRequestDetails = document.querySelector(Config.gitlabDomClasses.MR_DETAILS)

  if (mergeRequestList || mergeRequestDetails) {
    const store = await new Promise(resolve => chrome.storage.sync.get('store', async result => resolve(result.store)))
    storage = store ? store : getDefaultStorage()

    loadCustomCss()

    // page: merge requests list
    if (mergeRequestList) {
      // execute these only once, it's common between each merge request
      let currentUserName = ''
      let teammates = []
      if (storage.features.authors.active) {
        try {
          currentUserName = document.querySelector(Config.gitlabDomClasses.CURRENT_USERNAME).innerText.trim().toLowerCase()
        } catch (e) {
          console.error(`User name could not be found at "${Config.gitlabDomClasses.CURRENT_USERNAME}"`, e)
        }
        teammates = storage.features.authors.options.teammates.split(',').map(el => el.trim().toLowerCase()) || []
      }
      let projectId = 0
      if (storage.features.tooltip.active) {
        projectId = await getProjectId(mergeRequestList)
      }

      // execute on each merge request
      mergeRequestList.querySelectorAll(Config.gitlabDomClasses.MR_ELEMENT)
        .forEach(async mergeRequest => {
          if (storage.features.ticketing.active) {
            const linkText = mergeRequest.querySelector(Config.gitlabDomClasses.MR_LINK).textContent.trim()
            const element = mergeRequest.querySelector(Config.gitlabDomClasses.MR_VOTES_CONTAINER)
            showTicketButtonInListPage(element, linkText)
          }

          if (storage.features.authors.active) {
            highlightAuthors(mergeRequest, currentUserName, teammates)
          }

          if (storage.features.coloring.active || storage.features.tooltip.active) {
            const mergeRequestLinkElement = mergeRequest.querySelector(Config.gitlabDomClasses.MR_LINK)

            const stateRegexes = getMergeRequestRegexMatch(mergeRequestLinkElement)

            if (storage.features.coloring.active) {
              // quick coloring based on thumbsup shown in current page, before calling endpoints
              const mergeRequestStateSimulation = {
                thumbsup: {
                  nb: 0
                },
                thumbsdown: {
                  nb: 0
                },
                nbThreads: 0,
                allThreadsResolved: true,
                ...stateRegexes
              }
              quickColorizeMergeRequestLink(mergeRequest, mergeRequestStateSimulation)
            }

            loading(mergeRequest, mergeRequestLinkElement, true)
            const stateThreads = await getThreadsState(mergeRequestLinkElement)
            const stateApprovals = await getApprovalsState(mergeRequestLinkElement, projectId)
            loading(mergeRequest, mergeRequestLinkElement, false)

            const mergeRequestState = { ...stateThreads, ...stateApprovals, ...stateRegexes }

            if (storage.features.coloring.active) {
              colorizeMergeRequestLink(mergeRequestLinkElement, mergeRequestState)
            }

            if (storage.features.tooltip.active) {
              mergeRequest.title = buildMergeRequestTooltip(mergeRequestState)
            }
          }
        })
    }

    // page: merge request details
    else if (mergeRequestDetails) {
      if (storage.features.ticketing.active) {
        const title = mergeRequestDetails.querySelector(Config.gitlabDomClasses.MR_DETAILS_TITLE)
        showTicketButtonInDetailsPage(title, title.innerText)
      }
    }
  }
}
